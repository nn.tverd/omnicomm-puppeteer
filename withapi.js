const { default: axios } = require("axios");
const urlForReport = "https://script.google.com/macros/s/AKfycbyg8t3eS9ucyGTlWo5WXpu56qxdlMFbApl3kZFQ5lNKsOPYcAE/exec"

function convertSecToString(sec) {
  let string = "";
  const hh = Math.floor(sec / (60 * 60));
  const secForMin = sec % (60 * 60);
  const mm = Math.floor(secForMin / 60);
  const ss = secForMin % 60;
  string = `${hh}:${mm}:${ss}`;
  return string;
}
function convertMinToString(min) {
  let string = "";
  const hh = Math.floor(min / (60));
  const minForMin = min % (60);
  const mm = minForMin;
  const ss = "00";
  string = `${hh}:${mm}:${ss}`;
  return string;
}

const auth = async (_urlForReport) => {
  const base = "https://online.omnicomm.ru";
  const method = "/auth/login?jwt=1";
  const authResult = await axios({
    method: "post",
    url: base + method,
    data: {
      "login": "tsncompany",
      "password": "T8p6ezXPG"
    },
    headers: {
      "Content-Type": "application/json",
    },
  });
  console.log(authResult.data);

  const vehicleMethod = "/ls/api/v1/tree/vehicle"

  const vehivlesResult = await axios({

    method: "get",
    url: base + vehicleMethod,

    headers: {
      "Content-Type": "application/json",
      "Authorization": `JWT ${authResult.data.jwt}`
    },
  });
  console.log(vehivlesResult.data)



  // const statMethod = `/ls/api/v1/vehicles/${vehivlesResult.data.objects[0].uuid}/state`

  // const stat = await axios({

  //   method: "get",
  //   url: base + statMethod,

  //   headers: {
  //     "Content-Type": "application/json",
  //     "Authorization": `JWT ${authResult.data.jwt}`
  //   },
  // });
  // console.log(stat.data)

  let vehiclesList = "%5B";
  let comma = false;
  for (let i in vehivlesResult.data.objects) {
    if (comma) vehiclesList += ",";
    comma = true;
    const uuid = vehivlesResult.data.objects[i].terminal_id;
    vehiclesList += uuid;
  }
  vehiclesList += "%5D";
  const date2 = new Date();
  const date = new Date(date2.toISOString().slice(0, 10));

  // console.log(date, Math.floor(date.getTime() / 1000))
  // console.log(Math.floor(date2.getTime() / 1000))

  // const reportMethod = `/ls/api/v3/reports/statistics`
  const dataGroup = "%5Bmw,fuel,cmw,can,ccan,ae%5D"
  const reportMethod = `/ls/api/v1/reports/statistics?vehicles=${vehiclesList}&dataGroups=${dataGroup}&timeBegin=${Math.floor(date.getTime() / 1000)}&timeEnd=${Math.floor(date2.getTime() / 1000)}`
  // const reportMethod = `/ls/api/v1/reports/statistics?dataGroups=[mw]&timeBegin=${Math.floor(date.getTime() / 1000)}&timeEnd=${Math.floor(date2.getTime() / 1000)}`
  // console.log(reportMethod)
  const report = await axios({

    method: "get",
    url: base + reportMethod,

    headers: {
      "Content-Type": "application/json",
      "Authorization": `JWT ${authResult.data.jwt}`
    },
  });
  // console.log(report.data.data.vehicleDataList)
  const myReport = {
    source: "heroku2021-2",
    payload: [],
  }
  const date1_ = new Date();
  const date2_ = date1_.toISOString().slice(0, 10);
  for (let i in report.data.data.vehicleDataList) {
    const item = report.data.data.vehicleDataList[i];
    const row = {
      id: item.name,
      time: new Date(),
      date: date2_,
      mileage: item.mw.mileage, // Пробег (км)
      sredSkorostVDvijenii: "do not found", // Средняя скорость в движении, км/ч
      timeNoMoveSec: item.mw.workedNoMovement,
      workedNoMovementPercent: item.mw.workedNoMovementPercent,
      timeNoMove: convertSecToString(item.mw.workedNoMovement) + ` (${Math.round(item.mw.workedNoMovementPercent)})`,
      mileageAtPeriodBegin: item.cmw.mileageAtPeriodBegin, //Общий пробег на начало периода (км)
      engineOperationMin: item.can.engineOperationMin, //Моточасы (мин)
      engineOperation: convertMinToString(item.can.engineOperationMin),
      normalRPMSec: item.mw.normalRPM, // Время работы двигателя под нагрузкой, час:мин:сек, (% от времени работы двигателя)
      normalRPMPercent: item.mw.normalRPMPercent,
      normalRPM: convertSecToString(item.mw.normalRPM) + ` (${Math.round(item.mw.normalRPMPercent)})`, // Время работы двигателя под нагрузкой, час:мин:сек, (% от времени работы двигателя)
      mileageAtPeriodEnd: item.cmw.mileageAtPeriodEnd, //Общий пробег на начало периода (км)
      startVolume: item.fuel.startVolume / 10, // Начальный объём (дл)
      fuelConsumption: item.fuel.fuelConsumption / 10, // Фактический расход (дл)
      draining: item.fuel.draining / 10, // Объём сливов (дл)
      endVolume: item.fuel.endVolume / 10, // Конечный объём (дл)
      refuelling: item.fuel.refuelling / 10, // Объём заправок (дл)
      fuelConsumptionOnWorked: item.fuel.fuelConsumptionOnWorked / 10, // Фактический расход за время работы двигателя (дл)
      workedSec: item.mw.worked, // Время работы двигателя (с)
      workedPercent: item.mw.workedPercent,
      worked: convertSecToString(item.mw.worked) + ` (${Math.round(item.mw.workedPercent)})`,
      layUpSec: item.mw.layUp, // Время с выключенным двигателем (с)
      layUpPercent: item.mw.layUpPercent,
      layUp: convertSecToString(item.mw.layUp) + ` (${Math.round(item.mw.layUpPercent)})`,
      excessRPMSec: item.mw.excessRPM, // Время с выключенным двигателем (с)
      excessRPM: convertSecToString(item.mw.excessRPM),
      spn246: item.ccan.spn246, // Значение одометра CAN на начало периода (км)

    }
    console.log(row);
    myReport.payload.push(row);
  }
  if (!_urlForReport) {
    _urlForReport = urlForReport;
  }
  await axios({
    method: "post",
    url: urlForReport,
    data: myReport,
    headers: {
      "Content-Type": "text/plain;charset=utf-8",
    },
  });
}
module.exports = auth;