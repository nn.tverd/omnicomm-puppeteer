// if there is no search button to click we shoul skip this item
// 96243577

// https://stackoverflow.com/questions/52225461/puppeteer-unable-to-run-on-heroku
const puppeteer = require("puppeteer");
const fs = require("fs");
const axios = require("axios");

const links = require("./credentials/links.json");
const credentials = require("./credentials/credentials.json");
const cookie = {};
// const cookie = require("./credentials/omnicommCookie.json");
//-------------------------------------------------------------------------------
const refs1 = {
    "Пробег, км": 0,
    "Средняя скорость в движении, км/ч": 1,
    "Время работы двигателя без движения, час:мин:сек, (% от периода отчета)": 2,
    "Общий пробег на начало периода, км": 3,
    "Время работы двигателя (моточасы), час:мин:сек, (% от периода отчета)": 4,
    "Время работы двигателя под нагрузкой, час:мин:сек, (% от времени работы двигателя)": 5,
    "Общий пробег на конец периода, км": 6,
    "Начальный объём, л": 7,
    "Фактический расход, л": 8,
    "Объём сливов, л": 9,
    "Конечный объём, л": 10,
    "Объём заправок, л": 11,
    "Фактический расход за время работы двигателя, л": 12,
    "Время работы, час:мин:сек": 13,
    "Время простоя, час:мин:сек": 14,
    "Время работы выше допустимого значения, час:мин:сек": 15,
    "Значение одометра CAN на начало периода, км": 16,
};
const refs2 = {};
for (let key in refs1) {
    refs2[refs1[key]] = key;
}
//-------------------------------------------------------------------------------
// const sleep = require("util").promisify(setTimeout)(
function timeout(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
//-------------------------------------------------------------------------------
async function autoScroll(page) {
    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            var totalHeight = 0;
            var distance = 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if (totalHeight >= scrollHeight) {
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
}
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
async function isVisible(page, selector) {
    return await page.evaluate((selector) => {
        var e = document.querySelector(selector);
        if (e) {
            var style = window.getComputedStyle(e);

            return (
                style &&
                style.display !== "none" &&
                style.visibility !== "hidden" &&
                style.opacity !== "0"
            );
        } else {
            return false;
        }
    }, selector);
}
//-------------------------------------------------------------------------------
async function typeInSearch(page, partNumber) {
    await page.$eval("#query-search", (el) => (el.value = ""));
    await page.type("#query-search", `${partNumber}`, {
        delay: 30,
    });
    await page.click("#search-btn");
    await page.waitForSelector("#table-results-search-container");
    await page.waitForTimeout(1500);
    return page;
}
//-------------------------------------------------------------------------------
async function clickMoreArticles(page) {
    if ((await page.$("#more-articles")) !== null) {
        if (await isVisible(page, "#more-articles")) {
            console.log("found more-articles");
            await page.click("#more-articles");
            await page.waitForSelector("#table-results-search-container");
            await page.waitForTimeout(1500);
        }
    }
}
//-------------------------------------------------------------------------------
async function loginToOmnicomm(page) {
    if (Object.keys(cookie).length) {
        // console.log(cookie[0].expires, new Date().getTime());
        // console.log(new Date(cookie[0].expires), new Date());
        for (let cook of cookie) {
            if (cook.expires && cook.expires * 1000 > new Date().getTime()) {
                // console.log("use cookie .... ");
                await page.goto(start_url, { waitUntil: "networkidle2" });
                await page.setCookie(...cookie);
                return null;
            }
        }
    }
    console.log("login .... ");

    await page.goto(start_url, { waitUntil: "networkidle2" });
    await page.waitForSelector("form[name='loginForm']");
    const loginInp = await page.$('input[name="login"]');
    await loginInp.type(credentials.login, { delay: 30 });
    const passwordInp = await page.$('input[name="password"]');
    await passwordInp.type(credentials.password, { delay: 30 });

    const submitBtn = await page.$('button[type="submit"]');
    await submitBtn.click();
    // await page.click("#enter");

    await page.waitForNavigation({ waitUntil: "networkidle0" });
    await page.waitForTimeout(500);

    try {
        await page.waitForSelector("#omnicomm-online");
    } catch (error) {
        console.log(error);
        process.exit(0);
    }
    const currentCookie = await page.cookies();

    const cookieToWrite = [];
    for (let i in currentCookie) {
        // if (currentCookie[i].name === "ci_sessions") {
        cookieToWrite.push(currentCookie[i]);
        // }
    }

    // console.log("currentCookie", currentCookie);
    fs.writeFileSync(
        "./credentials/omnicommCookie.json",
        JSON.stringify(cookieToWrite)
    );
}
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
async function selectStat(page) {
    await page.click(".openNewTab-tip");
    await page.waitForSelector("#groupstat"); //#groupstat
    await page.click("#groupstat");
}
//-------------------------------------------------------------------------------
async function getTransport(page) {
    const data = [];
    try {
        await page.waitForTimeout(500);
        console.log("...get transport...");
        // const gloriousTree = await page.$$("#gloriousTree");
        const transports = await page.$$("span.gloriousTitle");
        const startDateCont = await page.$$("div.date-container");
        // const _startDate = await startDateCont.$$("div.date-container");
        const startDate = await (
            await startDateCont[0].getProperty("innerText")
        ).jsonValue();
        console.log("transports", transports.length);
        // const elHandleArray = await page.$$("button");
        for (const el of transports) {
            await el.click();
            await page.waitForTimeout(2500);
            console.log("there was click...");
            const element = {
                date: startDate,
                time: new Date(),
                refs1: refs1,
                refs2: refs2,
            };
            element.id = await (await el.getProperty("innerText")).jsonValue();
            // await (await tweets[i].getProperty('innerText')).jsonValue()
            const payload = await page.evaluate(() => {
                const _element = {};
                try {
                    const reportBody = document.getElementsByClassName(
                        "omn-report-body"
                    )[0];
                    const sections = reportBody.querySelectorAll(
                        ".groupstat-panel"
                    );
                    console.log(sections.length);
                    for (let section of sections) {
                        const rows = section.querySelectorAll(".groupstat-row");
                        for (let row of rows) {
                            const cols = row.querySelectorAll(".groupstat-col");
                            for (let col of cols) {
                                const _label = col.querySelector(
                                    ".groupstat-label"
                                );
                                const _value = col.querySelector(
                                    ".groupstat-value"
                                );
                                if (_label && _value) {
                                    const label = _label.innerText;
                                    const value = _value.innerText;
                                    _element[label] = value;
                                    console.log(label, value);
                                }
                            }
                        }
                    }
                    return _element;
                } catch (error) {
                    console.log(error);
                }

                return 0;
            });
            element.payload = JSON.parse(JSON.stringify(payload));
            data.push(element);
        }
    } catch (e) {
        console.log(e);
        return -1;
    }
    return data;
}
//-------------------------------------------------------------------------------

const start_url = links.root;

async function MyFunc() {
    console.log("omnicommDownloader.....");
    const startDate = new Date();

    // puppeteer.launch({devtools: true})
    const browser = await puppeteer.launch({
        headless: true,
        devtools: false,
        args: ["--no-sandbox", "--disable-setuid-sandbox"],
    });

    const page = await browser.newPage();

    await page.setViewport({
        width: 1920,
        height: 10800,
    });

    await page.emulateTimezone("Asia/Almaty");

    await loginToOmnicomm(page);
    await selectStat(page);
    const transports = await getTransport(page);
    console.log(transports);
    browser.close();
    return transports;
}

// MyFunc();

module.exports = MyFunc;
