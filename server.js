const express = require("express");
const bodyParser = require("body-parser");
const __main = require("./app.js");
const withApi = require("./withapi.js");
const { default: axios } = require("axios");

const app = express();
console.log("app...");
// app.set("view engine", "ejs");

app.use(bodyParser.json());
app.use(express.urlencoded({ extended: false }));

app.post("/", async (req, res) => {
    console.log("app.post / ");
    const urlToAnswer = req.body.thisAppUrl;
    res.json({ status: "ok" });
    const main = await __main();
    const resp = {
        source: "heroku2021",
        payload: main,
    };
    // main.source = "heroku2021";
    console.log(resp);
    axios({
        method: "post",
        url: urlToAnswer,
        data: resp,
        headers: {
            "Content-Type": "text/plain;charset=utf-8",
        },
    });
    // return res.json(main);
});

app.get("/", async (req, res) => {
    console.log("app.post / ");
    const urlToAnswer = req.body.thisAppUrl;
    res.json({ status: "ok" });
    const main = await __main();
    const resp = {
        source: "heroku2021",
        payload: main,
    };
    // main.source = "heroku2021";
    console.log(resp);
    axios({
        method: "post",
        url: urlToAnswer,
        data: resp,
        headers: {
            "Content-Type": "text/plain;charset=utf-8",
        },
    });
    // return res.json(main);
});


app.get("/withapi", async (req, res) => {
    console.log("app.post /with api ");
    const urlToAnswer = req.body.thisAppUrl;
    res.json({ status: "ok" });
    console.log(urlToAnswer)
    await withApi(urlToAnswer);
});

app.listen(process.env.PORT || 8080);
